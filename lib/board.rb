require 'byebug'
class Board

  attr_reader :grid

  def self.default_grid
    grid = []
    10.times { grid << [] }
    grid.each do |row|
      10.times { row << nil }
    end
  end

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def display
    grid
  end

  def count
    grid.flatten.count { |pos| pos == :s }
  end

  def populate_grid
    
  end

  def place_random_ship
    raise "ERROR!" if full?
    grid[rand(grid.length)][rand(grid.length)] = :s
  end

  def in_range?(pos)
    if grid.size >= pos[0] && grid.size >= pos[1]
      return true
    end
    false
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, value)
    x, y = pos
    grid[x][y] = value
  end

  def empty?(pos = nil)
    if pos.nil?
      grid.flatten.compact.empty?
    else
      grid[pos[0]][pos[1]].nil?
    end
  end

  def full?
    no_nils = grid.flatten.compact
    return false if grid.flatten.length > no_nils.length
    true
  end

  def won?
    count = grid.flatten.count { |pos| pos == :s }
    return true if count == 0
    false
  end

end
