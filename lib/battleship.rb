# require './board.rb'
# require './player.rb'
class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.default_grid)
    @player = player
    @board = board
  end

  def play
    play_turn until game_over?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

  def attack(pos)
    board[pos] = :x
  end

  def count
    board.count
  end

  def display_status
    board.grid
  end

  def game_over?
    board.won?
  end

end

# newnew = BattleshipGame.new('mashu')
# p newnew.play
